@extends('adminLTE.master')

@section('judul')
    Home
@endsection

@section('content')
    <div class="px-3">
        <h1>SanberBook</h1>
        <h3>Social Media Developer Santai Berkualitas</h3>
        <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
        <h3>Benefit Join SanberBook</h3>
        <ul>
            <li>Mendapatkan motivasi dari sesama developer</li>
            <li>Sharing knowledge dari para mastah Sanber</li>
            <li>Dibuat oleh calon web developer terbaik</li>
        </ul>
        <h3>Cara Bergabung ke SanberBook</h3>
        <ol>
            <li>Mengunjungi Website ini</li>
            <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
            <li>Selesai</li>
        </ol>
    </div>

    <div class="alert alert-info alert-dismissible fade show" role="alert">
        <strong>Note : </strong>Untuk memeriksa <strong>tables</strong> dan <strong>data tables</strong> dapat dilakukan
        dengan mengklik <strong>sidebar table</strong>.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endsection
