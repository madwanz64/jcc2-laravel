@extends('adminLTE.master')

@section('judul')
    Register
@endsection

@section('content')
    <div class="px-3">
        <h1>Buat Account Baru</h1>
        <h3>Sign Up Form</h3>
        <div class="row">
            <div class="col-lg-6">
                <form action="/welcome" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="firstname">First Name:</label>
                                <input type="text" name="firstname" class="form-control" id="firstname">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="lastname">Last Name:</label>
                                <input type="text" name="lastname" class="form-control" id="lastname">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <label for="inputEmail4">Gender :</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="gender" id="genderMan" value="man"
                                    checked>
                                <label class="form-check-label" for="genderMan">
                                    Man
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="gender" id="genderWoman" value="woman">
                                <label class="form-check-label" for="genderWoman">
                                    Woman
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="gender" id="genderOther" value="other"
                                    disabled>
                                <label class="form-check-label" for="genderOther">
                                    Other
                                </label>
                            </div>
                            <label for="nationality">Nationality :</label>
                            <select class="form-control" name="nationality" id="nationality">
                                <option value="Indonesia">Indonesia</option>
                                <option value="Singapura">Singapura</option>
                                <option value="Malaysia">Malaysia</option>
                                <option value="Thailand">Thailand</option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <label for="language">Language Spoken:</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="language[]" id="languageIndonesia"
                                    value="Indonesia"><label for="languageIndonesia">
                                    Bahasa
                                    Indonesia</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="language[]" id="languageEnglish"
                                    value="English"><label for="languageEnglish">
                                    English</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="language[]" id="languageArabic"
                                    value="Arabic"><label for="languageArabic">
                                    Arabic</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="language[]" id="languageJapanese"
                                    value="Japanese"><label for="languageJapanese">
                                    Japanese</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="biodata">Bio :</label>
                        <textarea class="form-control" name="biodata" id="biodata" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mb-2 w-100">Sign Up</button>
                </form>
            </div>
        </div>

    </div>
@endsection
